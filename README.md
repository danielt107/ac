# Descripción
En la documentación presente se describirán los elementos usados para ejecutar la simulación con dispositivos IoT, teniendo tanto sensores, sistemas embebidos, protocolos de conectividad y herramientas de analítica de datos, siendo las últimas ofrecidas por Azure.

# Sensores

El sensor BME280 es un dispositivo que se utiliza para medir temperatura, humedad y presión de un lugar en específico con gran precisión y a un bajo consumo energético, funciona a través de sensores independientes de temperatura, humedad y presión, los cuales tienen cada uno sus características, condiciones y limitaciones. Tiene un total de 8 pines los cuales tienen la funcion de alimentar, de emitir datos, de recibir datos, de configurar el reloj, entre otros, todos estos necesarios para cumplir con todas las funciones del sensor, a continuación, observamos la disposición de los pines y sus funciones. Este sensor funciona a través de una placa que está conformada por elementos tales como resistencias smd, capacitores smd y un chip “BME280” desarrollado por la empresa BOSCH donde se llevan a cabo todos los procesos para la medición de los datos. En el ejercicio en específico vemos como se conectan 4 pines en específico, VIN y GND para alimentar al sensor y SCK y SDI para configurar el reloj serial y la salida de datos seriales. 

Al igual que el diodo LED tiene unas especificaciones eléctricas para el óptimo funcionamiento del sensor, pero en este caso a diferencia del diodo también tiene especificaciones para cada sensor el de presión, temperatura y humedad, donde se especifican datos como el rango al que opera, la estabilidad, la precisión absoluta, el ruido generado, entre otros. A continuación, se muestran estos datos del sensor BME280.

# Sistema embebido

Como ya sabemos un sistema embebido es un sistema de computación el cual es diseñado para poder ejecutar funciones específicas, sus componentes estan todos sobre una “placa base” y su procesamiento se lleva a cabo en un microcontrolador (microprocesador con interfaces de entradas y salidas una memoria pequeña). Dentro de las ventajas tenemos que estos sistemas pueden ser programados en el lenguaje ensamblador del microcontrolador o utilizando otros lenguajes como C/C++ mediante compiladores, otras de las muchas ventajas que trae consigo un sistema embebido como respuestas en tiempo real, corto tiempo de respuesta, reducción de costes, entre otros.

En este caso para el ejercicio el sistema embebido es una placa Rastberry Pi 3 la cual tiene un sistema Linux embebido en su totalidad, con el cual se puede desarrollar en la placa tal y como lo harías en un portátil, por lo que se podría decir que básicamente son un portátil muy pequeño, pero con el plus que tiene pines de entrada y salida para conectar otros componentes electrónicos, tales como el diodo LED y el sensor BME280.

# Código

Código
Al comenzar el código, lo primero que este hace es tener los ajustes necesarios para poder ejecutar su funcionalidad, ejecutando el código mostrado a continuación.
const wpi = require('wiring-pi'); //
const Client = require('azure-iot-device').Client;
const Message = require('azure-iot-device').Message;
const Protocol = require('azure-iot-device-mqtt').Mqtt;
const BME280 = require('bme280-sensor');

Wiring Pi es una librería de acceso a los GPIO (General Purpose Input Output), escrita en C y diseñada para ser usada con la Raspberry Pi, permitiendo leer y establecer los valores de entradas y salidas de la misma. La constante Client es una representación de la clase Client de Azure, usada para conectar el dispositivo de la simulación con un hub IoT de Azure, la constante Message representa a un mensaje de o para el mismo hub, la constante protocolo se refiere al protocolo usado para establecer la comunicación (enviar los mensajes), en este caso se usa MQTT (MQ Telemetry Transport), un protocolo de comunicación enfocado a la conectividad Machine-to-Machine de forma inalámbrica y basado en la pila TCP/IP, que permite establecer una comunicación de tipo “Publisher-subscriber”  por medio de un broker, siendo considerado una de las partes más importantes del IoT.
Por último, la constante BME280 representa un paquete de Nodejs, en el que se usa el estándar I2C para la comunicación entre el sensor de humedad, temperatura y presión barométrica y la Raspberry por medio de un solo bus, como se ve en el código a continuación.
const BME280_OPTION = {
  i2cBusNo: 1, // defaults to 1
  i2cAddress: BME280.BME280_DEFAULT_I2C_ADDRESS() // defaults to 0x77
};
El bus por el que se comunica es por default, el 1, y además, la librería presenta la dirección I2C del BME280, la cual es una dirección estándar, y corresponde a 0x76HEX, estos son los parámetros requeridos para que se pueda establecer la comunicación entre el sensor y la Raspberry. 
const connectionString = 'HostName=SEMESTRE8.azure-devices.net;DeviceId=1;SharedAccessKey=EB0TnXIacqIMPFUfql/TYowp83g4Nv4C0NfhCiEBjXk=';
const LEDPin = 4;

var sendingMessage = false;
var messageId = 0;
var client, sensor;
var blinkLEDTimeout = null;
Acto seguido, se establecen las variables connectionString y LEDPin, siendo la primera la cadena de conexión principal proveída por Azure al crear un dispositivo IoT, que se usa para poder establecer la comunicación entre Azure y la placa, en cuanto a la segunda, es el número del pin perteneciente a la salida para el LED.  Ya que se inicia el código, se establece que messageId sea 0 para que el id del primer mensaje recibido sea 1, además, ya que no se ha enviado mensaje alguno, se establece una variable sendingMessage con valor falso (que se usará a la hora de enviar el mensaje).
function getMessage(cb) {
  messageId++; //Se incrementa el número del id del mensaje
  sensor.readSensorData() //Se leen los datos del sensor
    .then(function (data) { //se obtienen los datos dados por el sensor 
      cb(JSON.stringify({ //Se hace una conversión de los datos recibidos a JSON (mostrados como Strings)
        messageId: messageId, //Id del mensaje del sensor
        deviceId: 'Raspberry Pi Web Client',
        temperature: data.temperature_C, //Temperatura obtenida
        humidity: data.humidity //Humedad obtenida
      }), data.temperature_C > 30);
    })
    .catch(function (err) { //Si se recibe un error es debido a que el sensor no pudo proveer el dato, de modo que se muestra un error relacionado a esto	
      console.error('Failed to read out sensor data: ' + err);
    });
}
Con la función getMessage se leen los datos obtenidos del BME280, y se asignan los valores en un archivo .JSON, si no se leen los datos, entonces se da un mensaje de error de lectura del sensor.
function sendMessage() {
  if (!sendingMessage) { return; } //Si se está enviando un mensaje en el momento, no sigue con el resto del código

  getMessage(function (content, temperatureAlert) {
    var message = new Message(content); //Se crea un objeto tipo mensaje con la información proveída por el sensor en getMessage()
    message.properties.add('temperatureAlert', temperatureAlert.toString());
    console.log('Sending message: ' + content); //Se imprime en consola que se está enviando el mensaje
    client.sendEvent(message, function (err) { //Se envía el mensaje al cliente establecido 
      if (err) {
        console.error('Failed to send message to Azure IoT Hub'); //Si falla el envoi, se muestra un error en pantalla
      } else {
        blinkLED(); //De lo contrario, se ejecuta el método blinkLED()
        console.log('Message sent to Azure IoT Hub'); // Además, se imprime que el mensaje ha sido enviado
      }
    });
  });
}
La función sendMessage() verifica si no hay algún mensaje enviándose en el momento, si es así, no ejecuta alguna otra acción más, de lo contrario, ejecuta el método getMessage() y obtiene el contenido del mismo, crea un objeto tipo message (de Azure) y pone la información en este, además, con el cliente que ya se ha establecido anteriormente, se envía el mensaje al hub IoT de Azure. Los métodos onStart y onStop son usados cuando se quiere establecer o terminar la comunicación con el hub, si se ejecutan correctamente muestran en pantalla que el envío de datos se ha iniciado o se ha detenido.
function onStart(request, response) {
  console.log('Try to invoke method start(' + request.payload + ')'); //Imprime en pantalla que se intenta invocar el método de inicio
  sendingMessage = true;

  response.send(200, 'Successully start sending message to cloud', function (err) { //Si se envía, se muestra un mensaje de éxito
    if (err) {
      console.error('[IoT hub Client] Failed sending a method response:\n' + err.message); //De lo contrario, se muestra que falló en establecer comunicación
    }
  });
}

function onStop(request, response) {
  console.log('Try to invoke method stop(' + request.payload + ')'); //Se imprime que se intenta invocar el método stop
  sendingMessage = false; //El estado de mensajes enviándose pasa a ser falso

  response.send(200, 'Successully stop sending message to cloud', function (err) { // Si se envía, se muestra un mensaje de éxito
    if (err) {
      console.error('[IoT hub Client] Failed sending a method response:\n' + err.message); //De lo contrario, se muestra que ocurrió un error al enviar el mensaje
    }
  });
}

function receiveMessageCallback(msg) {
  blinkLED();
  var message = msg.getData().toString('utf-8'); // Se reciben los datos del mensaje en forma de String con formato utf-8
  client.complete(msg, function () {
    console.log('Receive message: ' + message); //Al recibir el mensaje, se muestra que se recibió y el contenido del mismo.
  });
}
Cada que se recibe un mensaje del cliente (Azure), se ejecuta el método receiveMessageCallback(), en el que se lee y muestra el contenido del mensaje obtenido. Por otro lado, blinkLED() hace que el LED se encienda por 500ms cada vez que el método se ejecuta.


function blinkLED() {
  // Light up LED for 500 ms
  if(blinkLEDTimeout) { //Esta variable sirve para determinar si el led está encendido o no
       clearTimeout(blinkLEDTimeout); 
   }
  wpi.digitalWrite(LEDPin, 1); //Se envía un 1 a la salida para el LED, de modo que este se encenderá
  blinkLEDTimeout = setTimeout(function () { // Una vez se enciende, se esperarán 500ms para volverlo a apagar
    wpi.digitalWrite(LEDPin, 0);
  }, 500);
}
Finalmente, ya teniendo y sabiendo cómo se usan cada uno de los métodos anteriores, se procede a ejecutar el código que hará que el sistema funcione como se quiere.
// set up wiring
wpi.setup('wpi'); //Se establece el funcionamiento del wpi y GPIO
wpi.pinMode(LEDPin, wpi.OUTPUT); //El pin del LED se establece como una salida
sensor = new BME280(BME280_OPTION); //Se crea un objeto del sensor con las variables de comunicación establecidas anteriormente
sensor.init() //Se inicia el sensor
  .then(function () {
    sendingMessage = true; //Se establece que el envío del mensaje esté activo
  })
  .catch(function (err) {
    console.error(err.message || err); //Si ocurre un error, se muestra en pantalla
  });

// create a client
client = Client.fromConnectionString(connectionString, Protocol); //Se crea un objeto tipo Client con el string dado para la conexión (cadena de conexión principal) y el protocolo MQTT

client.open(function (err) { //Se establece una conexión con el cliente
  if (err) {
    console.error('[IoT hub Client] Connect error: ' + err.message);
    return;
  }

  // set C2D and device method callback
  client.onDeviceMethod('start', onStart); //Si el método start se ejecuta en el cliente, también se hará en el dispositivo
  client.onDeviceMethod('stop', onStop);
  client.on('message', receiveMessageCallback); //Si se recibe un mensaje del cliente, se ejecutará receibeMessageCallback
  setInterval(sendMessage, 2000); // Se envía un mensaje cada 2 segundos con la información dada por el sensor (obtenida en getMessage)
});
En este declaran las variables y ejecutan los métodos necesarios para crear una conexión con el cliente, iniciar o parar el envío de mensajes, obtener estos y, por último, obtener los datos del sensor para ser enviados al hub.

# Conectividad

•	Conectividad física

Esta conectividad está presente entre la placa y el sensor y la placa y el LED que, para cuestiones del ejemplo, están conectados a la placa por medio de una protoboard. Los pines de cada uno de los elementos se unen por medio de cables, y la fuente de energía es proveída por la placa.

•	Protocolos de comunicación

Note que en la descripción del código ya se habló acerca de protocolos para la comunicación entre el sensor y la Raspberry y entre la Raspberry y el hub de Azure.
Para la comunicación entre el sensor y el LED con la placa se usa Wiring Pi, librería que incluye utilidades GPIO que se pueden usar para programar y establecer los valores de los pins GPIO, en los que se puede ejecutar la recepción y envío de datos análogos junto con la implementación sencilla de otros A/D o D/A. Junto a esto se utiliza el estándar I2C, en el que existen dos señales principales: SCL (System Clock), que es la línea de los pulsos de reloj que sincronizan el sistema, y SDA (System Data), que es la línea por la que se mueven los datos entre los dispositivos. Cuando se ejecuta la comunicación maestro-esclavo, SCL permanece alto, mientras que SDA permanece estable y sigue con la transmisión de información, una vez se termina la comunicación SCL vuelve a estar en bajo. Como se pueden conectar varios dispositivos con este estándar, una vez se quiere iniciar la comunicación SCL cambia su estado a “alto” (1), y se transmite un byte con 7 bits que aclaran a qué dispositivo se quiere conectar, y un octavo con la acción que se quiere realizar (lectura o escritura), con lo que se hace efectiva la comunicación incluso cuando hay más de un sensor.

Por último, y como también se describió en la sección del código, el protocolo MQTT es el usado para compartir información entre el dispositivo IoT y el hub, que también cuenta con seguridad como el transporte SSL/TLS y la autenticación mediante usuario y contraseña o certificado. Los mensajes MQTT están compuestos por 1 byte de control header, de 1 a 4 bytes con el largo del paquete, una cabecera opcional y un payload (que puede estar entre los 0 y los 256Mb), siendo este el mensaje que se envía. 

Para efectuar la comunicación entre el cliente y el bróker (o el servidor central), el primero puede enviar distintos tipos de mensajes, algunos de ellos son:
•	Connect: Mensaje con información necesaria para establecer conexión (usuario, contraseña), recibe un mensaje Connack, que contiene el resultado de la conexión.
•	Subscribe: Junto a unsubscribe, permite que el cliente se subscriba o retire su subscripción del bróker, recibe mensajes Suback o Unsuback
•	Publish: Envía los mensajes con contenido, siendo conformado por el topic y el payload

Con lo anterior se puede establecer la conexión entre el dispositivo IoT y el hub de Azure, con una conexión de tipo M2M.

# Data Analytic

Para la conexión con el hub IoT de Azure se siguieron los pasos dados en [1], donde se le dio el nombre “SEMESTRE8” al centro de IoT, y se creó un dispositivo IoT asignado como “1”, tal y como se muestra en la figura 9. Este presenta una clave de conexión principal con la que se puede conectar la Raspberry simulada como dispositivo IoT al hub.
El panel principal de este centro de IoT permite ver las estadísticas de los dispositivos IoT conectados y los mensajes enviados y recibidos, así como se ve en la figura 10, sin embargo, no es posible ver los mensajes que se reciben. Para esto, se encontró una manera en la que se permite ver el contenido de los mensajes en formato JSON, todo esto mediante Visual Studio Code y siguiendo los pasos que se muestran en [2] (figura 11), al igual que se muestra en las salidas de terminal de la Raspberry (figura 12).

Para poder ver toda la documentación con imágenes, acceda al PDF que se encuentra en el repositorio.
